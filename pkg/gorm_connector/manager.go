package gorm_connector

import (
	"database/sql"
	"fmt"
	"gitlab.com/mmorgenstern/pggis/pkg/gopher_help"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"time"
)

//Manager holds all Connections to Databases
type Manager struct {
	connections map[string]*gorm.DB
}

//NewManager create an new Instance of the Manager
func NewManager() *Manager {
	return &Manager{
		connections: make(map[string]*gorm.DB),
	}
}

//CreateConnection creates a new Connection and adds it to the List of Connections.
//the Connection was opened
func (dc *Manager) CreateConnection(name, connectionString string) *gorm.DB {
	if dc.connections[name] != nil {
		dc.DeleteConnection(name)
	}
	dc.connections[name] = connect(connectionString)
	return dc.connections[name]
}

//DeleteConnection close the Connection to the Database and remove it from the List of Connections
func (dc *Manager) DeleteConnection(name string) {
	disconnect(dc.GetConnection(name))
	delete(dc.connections, name)
}

//GetConnection give a Connection by its name
func (dc *Manager) GetConnection(name string) *gorm.DB {
	if dc.connections[name] == nil {
		panic(fmt.Sprintf("missing connection with name %v", name))
	}
	return dc.connections[name]
}

func connect(connectionString string) *gorm.DB {
	db, err := gorm.Open(postgres.Open(connectionString), &gorm.Config{})
	gopher_help.PanicOnError(err)

	sqlDb := getSqlDb(db)
	sqlDb.SetMaxIdleConns(10)
	sqlDb.SetMaxOpenConns(50)
	sqlDb.SetConnMaxLifetime(time.Hour)
	return db
}

func disconnect(db *gorm.DB) {
	if db == nil {
		return
	}

	sqlDb := getSqlDb(db)
	gopher_help.PanicOnError(sqlDb.Close())
}

func getSqlDb(db *gorm.DB) *sql.DB {
	gopher_help.PanicIfNil(db, "no db instance available!")
	tmp, err := db.DB()
	gopher_help.PanicOnError(err)
	return tmp
}