package pginformations

type Columns struct {
	Catalog string `gorm:"column:table_catalog"`
	Schema string `gorm:"column:table_schema"`
	Table string `gorm:"column:table_name"`
	Column string `gorm:"column:column_name"`
	OrdinalPosition int `gorm:"column:ordinal_position"`
	ColumnDefault string `gorm:"column:column_default"`
	IsNullable string `gorm:"column:is_nullable"`
	DataType string `gorm:"column:data_type"`
	UdtName string `gorm:"column:udt_name"`
}

func (Columns) TableName() string {
	return "information_schema.columns"
}
