package pginformations

import (
	"gitlab.com/mmorgenstern/pggis/pkg/gorm_connector"
	"testing"
)

func DbManager() *gorm_connector.Manager {
	m := gorm_connector.NewManager()
	m.CreateConnection("test", "host=agrodb user=dboperator password=dbugis dbname=agrodb_49 port=5432 sslmode=disable")
	return m
}

func TestTableContext_GetGeomTables(t *testing.T) {
	ctx := NewTableContext(DbManager())
	columns := ctx.GetGeomTables("test")
	if len(columns) < 1 {
		t.Errorf("missing geom Columns in Database %v", "test")
	}
}
