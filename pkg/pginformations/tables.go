package pginformations

import "gitlab.com/mmorgenstern/pggis/pkg/gorm_connector"

type TableContext struct {
	manager *gorm_connector.Manager
}

func NewTableContext(manager *gorm_connector.Manager) *TableContext {
	return &TableContext{
		manager: manager,
	}
}

func (tc *TableContext) GetGeomTables(instance string) []*Columns {
	db := tc.manager.GetConnection(instance)
	var columns []*Columns
	db.Where(&Columns{UdtName: "geometry"}).Find(&columns)
	return columns
}