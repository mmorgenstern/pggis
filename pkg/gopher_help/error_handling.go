package gopher_help

func PanicOnError(err error) {
	if err != nil { panic(err) }
}

func PanicIfNil(value interface{}, msg string) {
	if value == nil {
		panic(msg)
	}
}
